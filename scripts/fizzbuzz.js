/* File: fizzbuzz.js
 * Author:
 */

/*
Description: Now modify the program to print "FizzBuzz" for numbers that are divisible by both 3 and 5 (and still print "Fizz" or "Buzz" for numbers divisible by only one of those).  Keep count of how many times "Fizz", "Buzz" and "FizzBuzz" are written, and write those counts out to the document. (5 marks)
*/
